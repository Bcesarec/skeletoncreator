﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.IO;

public class GameController: MonoBehaviour
{
    public GameObject startScreen;
    public GameObject endScreen;
    public GameObject pauseScreen;
    public GameObject CharacterCreator;
    public static bool GameisPaused = false;
    public bool canResume;
    public UnityEvent resetGame;
    public Camera Main_Camera;
    public Camera Secondary_Camera;
    bool _inStartScreen;
    public RenderTexture rendTex;
    private static int imageCount = 00;

    public GameObject HatOne;
    public GameObject HatTwo;
    public GameObject HatThree;
    public GameObject HatFour;
    public GameObject HatFive;

    public GameObject CastleWall;
    public GameObject Mountain;
    public GameObject Desert;

    bool _BG1;
    bool _BG2;
    bool _BG3;

    bool _Hat1;
    bool _Hat2;
    bool _Hat3;
    bool _Hat4;
    bool _Hat5;

    // Start is called before the first frame update
    void Start()
    {
        startScreen.SetActive(true);
        endScreen.SetActive(false);
        pauseScreen.SetActive(false);
        CharacterCreator.SetActive(false);
        Main_Camera.enabled = true;
        Secondary_Camera.enabled = false;
        _inStartScreen = true;

        _Hat1 = false;
        _Hat2 = false;
        _Hat3 = false;
        _Hat4 = false;
        _Hat5 = false;
        _BG1 = false;
        _BG2 = false;
        _BG3 = false;
        HatOne.SetActive(false);
        HatTwo.SetActive(false);
        HatThree.SetActive(false);
        HatFour.SetActive(false);
        HatFive.SetActive(false);
        CastleWall.SetActive(false);
        Mountain.SetActive(false);
        Desert.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (GameisPaused)
            {
                if (canResume)
                {
                    Resume();
                }
            }
            else
            {
                Pause();
            }
        }
    }

    public void StartGame()
    {
        startScreen.SetActive(false);
        endScreen.SetActive(false);
        pauseScreen.SetActive(false);
        CharacterCreator.SetActive(true);
        _inStartScreen = false;

        HatOne.SetActive(false);
        HatTwo.SetActive(false);
        HatThree.SetActive(false);
        HatFour.SetActive(false);
        HatFive.SetActive(false);

        CastleWall.SetActive(false);
        Mountain.SetActive(false);
        Desert.SetActive(false);

        _Hat1 = false;
        _Hat2 = false;
        _Hat3 = false;
        _Hat4 = false;
        _Hat5 = false;

        _BG1 = false;
        _BG2 = false;
        _BG3 = false;
    }

    public void Resume()
    {
        pauseScreen.SetActive(false);
        CharacterCreator.SetActive(true);
        Time.timeScale = 1f;
        GameisPaused = false;
        Main_Camera.enabled = true;
        Secondary_Camera.enabled = false;
    }

    void Pause()
    {
        if(_inStartScreen)
        {
            return;
        }
        else
        {
          pauseScreen.SetActive(true);
          CharacterCreator.SetActive(false);
          Time.timeScale = 0f;
          GameisPaused = true;
          Main_Camera.enabled = false;
          Secondary_Camera.enabled = true;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameisPaused = false;

        HatOne.SetActive(false);
        HatTwo.SetActive(false);
        HatThree.SetActive(false);
        HatFour.SetActive(false);
        HatFive.SetActive(false);

        _Hat1 = false;
        _Hat2 = false;
        _Hat3 = false;
        _Hat4 = false;
        _Hat5 = false;

        CastleWall.SetActive(false);
        Desert.SetActive(false);
        Mountain.SetActive(false);

        _BG1 = false;
        _BG2 = false;
        _BG3 = false;
    }

    public void CaptureScreenshot()
    {
        Main_Camera.enabled = false;
        Secondary_Camera.enabled = true;
        CharacterCreator.SetActive(false);
        endScreen.SetActive(true);
        _inStartScreen = true;

        //Get the active render texture
        RenderTexture.active = rendTex;
        //Create a new 2D texture object that is the same width/height as our render texture
        Texture2D image = new Texture2D(rendTex.width, rendTex.height, TextureFormat.ARGB32, false);
        //Read the pixels of the Texture we just made
        image.ReadPixels(new Rect(0, 0, rendTex.width, rendTex.height), 0, 0);
        //Apply those pixels to the texture. Now we have the actual image we want to save out.
        image.Apply();

        //Read all the bytes of the image and encode them into a JPG. You can do other file formats, just search .EncodeTo___ in the intellisense popup
        byte[] bytes = image.EncodeToJPG();

        //Figure out where we want to save this
        //If you Debug Application.DataPath, it shows it leads to the Assets folder of our Unity project. This is true only for the editor. If you build out your project
        //Application.DataPath will lead to the "_data" folder in your build folder.
        //The entire directory path written here is "Assets/CaptureCamera_Images
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath + "/CaptureCamera_Images");

        //Does that directory exist? If no:
        if (!Directory.Exists(directory.ToString()))
        {
            Directory.CreateDirectory(directory.ToString());
        }
        //If yes:
        else
        {
            //Set our file name. The file will be saved with the name "Capture_0.jpg" right now
            string filename = "/Capture_" + imageCount.ToString() + ".jpg";

            //Get ghd file path we'll be using
            string filePath = Path.Combine(directory.ToString() + filename);

            Debug.Log(filePath);

            //This writes allll of the image information into it's own file and saves it to the preselected destination
            File.WriteAllBytes(filePath, bytes);
        }
        
    }
    public void PickHat()
    {
        if (_Hat1)
        {
            HatOne.SetActive(false);
            _Hat1 = false;
        }
        else
        {
            HatOne.SetActive(true);
            HatThree.SetActive(false);
            HatFour.SetActive(false);
            HatFive.SetActive(false);
            _Hat1 = true;
        }
    }

    public void PickBow()
    {
        if (_Hat2)
        {
            HatTwo.SetActive(false);
            _Hat2 = false;
        }
        else
        {
            HatTwo.SetActive(true);
            _Hat2 = true;
        }
    }

    public void PickArrows()
    {
        if (_Hat3)
        {
            HatThree.SetActive(false);
            _Hat3 = false;
        }
        else
        {
            HatThree.SetActive(true);
            HatOne.SetActive(false);
            HatFour.SetActive(false);
            HatFive.SetActive(false);
            _Hat3 = true;
        }
    }
    
    public void PickBanana()
    {
        if (_Hat4)
        {
            HatFour.SetActive(false);
            _Hat4 = false;
        }
        else
        {
            HatFour.SetActive(true);
            _Hat4 = true;

            HatOne.SetActive(false);
            HatThree.SetActive(false);
            HatFive.SetActive(false);
        }
    }

    public void PickFlower()
    {
        if (_Hat5)
        {
            HatFive.SetActive(false);
            _Hat5 = false;
        }
        else
        {
            HatFive.SetActive(true);
            _Hat5 = true;

            HatOne.SetActive(false);
            HatThree.SetActive(false);
            HatFour.SetActive(false);
        }
    }

    public void PickCastleWall()
    {
        if (_BG1)
        {
            CastleWall.SetActive(false);
            _BG1 = false;
        }
        else
        {
            CastleWall.SetActive(true);
            _BG1 = true;

            Desert.SetActive(false);
            Mountain.SetActive(false);
        }
    }

    public void PickMountain()
    {
        if (_BG2)
        {
            Desert.SetActive(false);
            _BG2 = false;
        }
        else
        {
            Desert.SetActive(true);
            _BG2 = true;

            CastleWall.SetActive(false);
            Mountain.SetActive(false);
        }
    }

    public void PickDesert()
    {
        if (_BG3)
        {
            Mountain.SetActive(false);
            _BG3 = false;
        }
        else
        {
            Mountain.SetActive(true);
            _BG3 = true;

            Desert.SetActive(false);
            CastleWall.SetActive(false);
        }
    }

    public void PickNoBG()
    {
        Mountain.SetActive(false);
        CastleWall.SetActive(false);
        Desert.SetActive(false);
        _BG1 = false;
        _BG2 = false;
        _BG3 = false;
    }
}

