﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DEMO_SaveCaptureImage : MonoBehaviour
{
    //Public variables
    public RenderTexture rendTex;

    //Private variables
    //This is static so it doesn't change on close.
    private static int imageCount = 00;

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Hit S and take a capture of the current render texture
        if (Input.GetKeyDown(KeyCode.S))
        {
            CaptureScreenshot();
        }
    }

    //Capture an image from what the camera sees
    public void CaptureScreenshot()
    {
        //Get the active render texture
        RenderTexture.active = rendTex;
        //Create a new 2D texture object that is the same width/height as our render texture
        Texture2D image = new Texture2D(rendTex.width, rendTex.height, TextureFormat.ARGB32, false);
        //Read the pixels of the Texture we just made
        image.ReadPixels(new Rect(0, 0, rendTex.width, rendTex.height), 0, 0);
        //Apply those pixels to the texture. Now we have the actual image we want to save out.
        image.Apply();

        //Read all the bytes of the image and encode them into a JPG. You can do other file formats, just search .EncodeTo___ in the intellisense popup
        byte[] bytes = image.EncodeToJPG();

        //Figure out where we want to save this
        //If you Debug Application.DataPath, it shows it leads to the Assets folder of our Unity project. This is true only for the editor. If you build out your project
        //Application.DataPath will lead to the "_data" folder in your build folder.
        //The entire directory path written here is "Assets/CaptureCamera_Images
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath + "/CaptureCamera_Images");

        //Does that directory exist? If no:
        if (!Directory.Exists(directory.ToString()))
        {
            Directory.CreateDirectory(directory.ToString());
        }
        //If yes:
        else
        {
            //Set our file name. The file will be saved with the name "Capture_0.jpg" right now
            string filename = "/Capture_" + imageCount.ToString() + ".jpg";
            
            //Get ghd file path we'll be using
            string filePath = Path.Combine(directory.ToString() + filename);

            Debug.Log(filePath);

            //This writes allll of the image information into it's own file and saves it to the preselected destination
            File.WriteAllBytes(filePath, bytes);
        }
    }
}
