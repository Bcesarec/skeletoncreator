﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BlendShapeController : MonoBehaviour {
    [SerializeField]
    SkinnedMeshRenderer BaseModel;

    [SerializeField]
    Transform torsoBone;
    [SerializeField]
    Transform headBone;
    float sliderValueBody;
    [SerializeField]
    ColorPicker colorPicker;

 
    public void SetSliderPrimaryColor(float sValue)
    {
        BaseModel.sharedMaterial.SetFloat("_Weight1", sValue);
    }
    public void SetSliderSecondaryColor(float sValue)
    {
        BaseModel.sharedMaterial.SetFloat("_Weight2", sValue);
    }
    public void SetSliderTertiaryColor(float sValue)
    {
        BaseModel.sharedMaterial.SetFloat("_Weight3", sValue);
    }


    public void SetSliderValueBody(float sValue)
    {
       
        BaseModel.SetBlendShapeWeight(3, sValue );
         BaseModel.SetBlendShapeWeight(4, -sValue);
    }

    public void SetSliderValueHead(float sValue)
    {

        BaseModel.SetBlendShapeWeight(1, sValue);
        BaseModel.SetBlendShapeWeight(2, -sValue);
    }


    public void SetTorsoSize(float sValue)
    {
        torsoBone.localScale = new Vector3(sValue, sValue, sValue);
    }

    public void SetHeadSize(float sValue)
    {
        headBone.localScale = new Vector3(sValue, sValue, sValue);
    }
}

